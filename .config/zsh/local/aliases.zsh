# {{{ Set my preferred editor
typeset -ga visual=(v)
# }}}
# {{{ stdin/stdout yank/paste
case ${WAYLAND_DISPLAY+w}${commands[wl-paste]+a}${DISPLAY+x}${commands[xclip]+a}${commands[xsel]+b} in
	wa*) alias y='wl-copy' p='wl-paste' Y='wl-copy -p' P='wl-paste -p' ;;
	*xa) alias y='xclip -sel clip -in' p='xclip -sel clip -out' Y='xclip -in' P='xclip -out' ;;
	*xb) alias y='xsel -b' p='xsel -b -o' Y='xsel' P='xsel -o' ;;
	*) alias {y,Y}='>> ~/.clip' {p,P}='cat ~/.clip'
esac
# }}}
# {{{ Quick access to zshrc, aliases, and history
ez(){
	local arg
	$visual $ZDOTDIR/(.|)*${^@}*
}
alias ezrc='$visual ${ZDOTDIR:-$HOME}/.zshrc'
alias ezshrc='$visual ${ZDOTDIR:-$HOME}/.zshrc'
function eal(){
	(( ${+1} )) &&
		$visual ${(%):-%x} -c "/\<$1=" ||
		$visual ${(%):-%x}
}
sched +0 compdef _aliases eal

alias ehist='$visual $HISTFILE'
alias h='history' a='alias'
# }}}
# {{{ kinda hacky:
# if so do-something
alias so='((!$?));'
# pass secret on command line
.zshrc.getpass(){
	typeset -g __PASS
	sched +0 unset __PASS
	read -s '__PASS?>'
}
functions -Ms __getpass 1 1 .zshrc.getpass
alias -g getpass='${$((__getpass())):+$__PASS}'
# Just show arguments
::(){
	(($#)) && printf '%q\n' "$@"
	:
}
# Choose something
sel(){
	local -i i=$@[(i)(-|--)]
	local REPLY x a=($@[1,i])
	shift i
	select x;$a $x
}
# Reply to args
alias r2a='set -- "${(@)reply}"'
# simple export wrapper for export1 NAME $~glob(Y1)
export1(){
  [[ $1 = [[:WORD:]]## ]] && export $1=$2
}
# }}}
# {{{ ls aliases
alias ls='ls --color=auto --classify --human-readable'
alias l=$aliases[ls]' -l'
alias lA=$aliases[ls]' --almost-all'
alias la=$aliases[ls]' --all'
alias lls=$aliases[ls]' -l --sort=size'
alias ll='ls -l'
alias lS=$aliases[ls]' --sort=size --size'
alias sl=$aliases[ls]' --reverse'     # no steam locomotive here

alias lt=$aliases[ls]' --sort=time'
alias lct=$aliases[lt]' --time=ctime'
alias lat=$aliases[lt]' --time=atime'
alias lT=$aliases[lt]' --reverse'
alias lcT=$alaisesl[lt]' --time=ctime --reverse'
alias laT=$aliases[lt]' --time=atime --reverse'
alias lsd='ls -C | column --output-width=$COLUMNS --table | lolcat'

function § { ls --color -C * $@ | less -R } # depth=1
# }}}
# {{{ sudo
alias _='sudo'
alias _i='sudo --login'
alias _u='sudo su'
alias _e='sudoedit'
compdef gksudo='sudo'
# }}}
# {{{ Grep aliases
if (( $+commands[rg] )); then
	# trim to $COLUMNS only when not in a pipeline and we have the space for a preview
	alias rg='rg --max-columns=$((COLUMNS > 60 && ! ZSH_SUBSHELL ? COLUMNS - 30 : 0))'
	alias rj='rg --json'
	rgl rgless RG (){ rg --pretty "$@" | less -RF }
	sched +0 compdef _rg {rgl,rgless,RG}='rg --pretty'
else
	alias grep='grep --color'
	# use grep as a cheap rg/ag replacemnet
	alias ag='grep -R -n -H --exclude-dir={.git,.svn,CVS}'
	alias rg='grep -R -n -H --exclude-dir={.git,.svn,CVS}'
	alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS}'
	grepl (){ grep --color=always "$@" | less -R }
	sched +0 compdef _grep grepl='grep --color=always'
fi
# }}}
# {{{ fd / find
alias find='noglob find'
if (( $+commands[fd] )); then
	alias f='noglob fd --glob'
	alias fd='noglob fd --glob --type directory'
	alias ff='noglob fd --glob --type file'
	alias fl='noglob fd --glob --type symlink'
else
	alias f='noglob find .'
	alias fd='noglob find . -type d -name'
	alias ff='noglob find . -type f -name'
	alias fl='noglob find . -type l -name'
fi
# }}}
alias m=run-help
alias z=zsh b=bash c=cat
T(){ tree -C "$@" | less -RF }
sched +0 compdef _tree T
alias mz='man zshall'
alias {Z,ZZ}='exec zsh'
# {{{ File operations
zmodload zsh/files # zsh versions of mv, rm, mkdir, etc
alias rm='rm -s'   # zsh extension, enable paranoid behavior
alias trash='gio trash'
alias cp='cp --interactive --reflink=auto' # lightweight copy when possible
alias mv='mv -i'
alias rscp='rsync -azP --human-readable --info=flist0,progress2,stats1'
function RSCP {
	rsync -aP "${1:-desktop.zt}:$(print -P %~)/${2:-.}/*" ${2:-.}
}
# }}}
# {{{ ZSH suffix magic
(){
	autoload -Uz zsh-mime-setup zsh-mime-handler
	[[ -r $1 ]] &&
		source $1
	if ! (( $+zsh_mime_handlers )); then
		zsh-mime-setup
		{
			alias -Ls
			typeset -p zsh_mime_handlers zsh_mime_flags
		} >| $1
	fi
} ${XDG_CACHE_HOME:-$HOME/.cache}/zsh-mime-cache.zsh
# open browser on urls
[[ "$BROWSER"  ]] && alias -s {htm,html,de,org,net,com,at,cx,nl,se,dk}=$BROWSER
alias -s {cpp,cxx,cc,c,hh,h,inl,asc,md,txt,TXT,tex}='$visual'
[[ "$XIVIEWER" ]] && alias -s {jpg,jpeg,png,gif,mng,tiff,tif,xpm}=$XIVIEWER

[[ $commands[mpv] ]] && alias -s {ape,avi,flv,m4a,mkv,mov,mp3,mp4,mpeg,mpg,ogg,ogm,rm,wav,webm}=mpv

#read documents
alias -s pdf=zathura ps=gv dvi=xdvi chm=xchm djvu=djview

#list whats inside packed file
alias -s zip="unzip -l" rar="unrar l" tar="tar tf" tar.gz="echo " ace="unace l"
# }}}
# {{{ Android / TTS
if (( ${+ANDROID_DATA} )); then
	alias tts=termux-tts-speak
	alias ssh=ssha
elif (( $+commands[pico2wave] )); then
	function tts() {
		local f=$(mktemp --suffix=.wav)
		trap '{ rm "$f"; exit }' INT
		pico2wave -w "$f" -- "$*"
		paplay "$f"
		rm "$f"
	}
	alias tts='noglob tts'
elif (( $+commands[mimic] )); then
	function tts() {
		mimic "$*" play
	}
	alias tts='noglob tts'
fi
# }}}
# {{{ Terminal utils
(){
	local field
	for field (r w x){
		alias -- "-$field=command chmod -$field"
		alias -- "+$field=command chmod u+$field,g+$field"
	}
}
# }}}
# {{{ git: use aliases in gitconfig
alias g='git'
alias gs='git s' # tired of ghostscript on accident
# }}}
# {{{ fzf / tmux
if (( ${+TMUX} )) && FZF_TMUX=1; then
	# Use tmux version if in tmux session
	alias fzf='fzf-tmux'

	# optional prefix
	:neww :new-window(){
		tmux "$0" "$@"
	}
fi
# fzf uses $SHELL to expand this
export FZF_DEFAULT_COMMAND='setopt extendedglob; p=${PWD:P}; locate "${p//(#m)\*\?\[\]/\\$MATCH}*" | cut -b$(( $#p + 2 ))-'
alias fzfa=$aliases[locate]' \* | fzf'
# }}}
# {{{ user-mount using udisksctl
alias ud=udisksctl
function udm {
	cd "$(udisksctl mount -b "$@" | tee /dev/stderr | sed 's/.* at \(.*\)$/\1/')"
}
function udum { # unmount by /dev/ or by mount path
	udisksctl unmount -b "$@" || udisksctl unmount -p "$@"
}
sched +0 compdef _mount udm=mount
sched +0 compdef _mount udum=umount
# }}}
# {{{ Some path functions
sched +0 compdef _files readlink
# if an executable expects to be launched in its directory
cdex() {
	local name
	[[ -f "$1" ]] && name="${1:P}" || name=${commands[$1]:-$1}
	cd "${name:P:h}"; "$name" "${@:2}"
}

# }}}
# {{{ grc colouring
alias GRC='grc -es'
alias LA='acol ls -lFAhb --color'
alias LS='acol ls -lFhb --color'
alias df='GRC df -hT'
alias dig='GRC dig'
alias docker='GRC docker'
alias docker-machine='GRC docker-machine'
alias env='acol env'
alias gcc='gcc -fdiagnostics-color=auto'
alias ip='ip -color=auto'
alias lsblk='acol lsblk'
alias lspci='acol lspci'
alias mount='acol mount'
alias lsmount='command mount | rg --color=never "^/" | acol -i -o mount'
alias nmap='acol nmap'
alias ping='GRC ping'
alias ps='GRC ps --columns $COLUMNS'
alias traceroute='GRC traceroute'
function lsl { grc ls -lFhb "$@" --color | less -R }
sched +0 compdef _ls lsl='ls -lFhb'
# }}}
# {{{ other colouring
function hi { highlight -O ansi "$@" 2>/dev/null | less -R -F }
# }}}
# {{{ UU: update everything
# tmux split git updates from package-manager udpates
if (( $+TMUX )); then
	if (( $+commands[pikaur] )); then
		alias UU='tmux split git-all -w pu; nice -n10 pikaur -Syu'
		alias pikaur='nice -n10 pikaur'
	elif (( $+commands[pkg] )); then
		alias UU='tmux split git-all -w pu; nice -n10 pkg update'
	elif (( $+commands[apt] )); then
		alias UU='tmux split git-all -w pu; sh -c "sudo apt update && sudo apt full-upgrade"'
	fi
	alias peek='tmux split-window -p 30 $EDITOR'
else
	if (( $+commands[pikaur] )); then
		alias UU='tmux new git-all -w pu \; split nice -n10 pikaur -Syu'
		alias pikaur='nice -n10 pikaur'
	elif (( $+commands[pkg] )); then
		alias UU='tmux new git-all -w pu \; split nice -n10 pkg update'
	elif (( $+commands[apt] )); then
		alias UU='tmux new git-all -w pu \; split sh -c "sudo apt update && sudo apt upgrade"'
	fi
fi
# }}}
# {{{ wrappers/defaults
function ncsh(){
	stty raw -echo
	ncat "$@"
	stty -raw echo
}
# Shortened python
alias py{,3}=python3

# Read comfortably
human-readable () {
	local TMPPREFIX=$XDG_RUNTIME_DIR/tmp TMPSUFFIX=.txt
	nvim +'set wrap linebreak nospell laststatus=0' ${@:-=(<&0)}
}
alias sys='systemctl --user'
alias Sys='systemctl'
alias jfeu='journalctl --user -xfeu'
alias Jfeu='journalctl -xfeu'
alias view=human-readable

alias nmtui='NEWT_COLORS="root=black,black;window=black,black;border=white,black;listbox=white,black;label=blue,black;checkbox=red,black;title=green,black;button=white,red;actsellistbox=white,red;actlistbox=white,gray;compactbutton=white,gray;actcheckbox=white,blue;entry=lightgray,black;textbox=blue,black" nmtui'
alias htip='HTOPRC=$HOME/.config/htop/minrc htop'
alias htap='HTOPRC=$HOME/.config/htop/maxrc htop'
alias htup='HTOPRC=$HOME/.config/htop/infrc htop'
alias ytdl='noglob youtube-dl'
alias ytdla='noglob yt-dlp -f ba/b -x'
alias pycalc='python3 -ic "
from math import *
import cmath as C
try:
    import numpy as np
except:
    pass
π, τ = pi, tau
i, j = 1j, 1j
"'
# ℝ by default
alias rust-c='rustc --out-dir build -O'
alias zcalc='IFS=, noglob zcalc'
function gc-c {
	local dir=${1:h}/build
	mkdir -p "$dir"
	gcc -O3 "$1" -o "$dir/${${1:t}%.*}" "${@:2}"
}
function gh-c { ghc -Odph -dynamic "$1" -o "build/${1%.*}" -odir .build -hidir .build -tmpdir . "${@:2}" }
function hscalc { echo -e "main=putStrLn $ show $ $@" | runhaskell }
alias qn="$visual ~/Sync/default/notes/quicknote.md"
function xevkb { xev -event keyboard "$@" | egrep -o '(keycode(.)+\)|XLookup.+[1-9].+)' }
compdef _x_utils xevkb='xev -event keyboard'
splitarray() { # Usage: splitarray name delimiter [string]
	if (($# < 2)); then
		print -l "Usage: $0 ARRAY_NAME DELIMITER [STRING]" '' \
		'If [STRING] is not provided, defaults to stdin'
		return
	fi
	typeset -a $1=("${(@ps[$2])"${3:-"$(<&0)"}"}")
}
']'(){
	local MATCH MBEGIN MEND
	[ "${(Oa@)argv[1,-2]}" "${argv[-1]//(#m)[\[\]]/${(#)$((6 ^ #MATCH))}}"
}
'='() case $1 {
	@|-|--) set -- "${@:2}" ;;
	-*) eval "typeset ${1}ga $2"'=("${@:3}")' ;;
	*) eval 'typeset -ga '$1'=("${@:2}")'
}
curls() { # for calling curl on multiple urls with the same options

	# Get start of flags/options
	local -i idx='argv[(i)-*]'
	if ((idx < 2)); then
		print -u2 "Usage: $0 [ URL [ URL ...] ] [[ CURL OPTS ]]"
		return 1
	fi

	# apply curl options to each url
	local curlopts=("${@[idx,-1]}") args=()
	local arg
	for arg (${@[1,idx-1]}) {
		args+=("${(@)curlopts}" $arg --next)
	}

	# remove last --next flag
	curl "${(@)args[1,-2]}"
}
# }}}
# {{{ *TeX
tex2txt(){
	local cmd=(pandoc -f latex -t plain)
	if (($#)); then
		$cmd <<< "\$$*\$"
	else
		$cmd
	fi
}
alias texmk='latexmk'         # default
alias texc='latexmk -c -norc' # leave output files (*.dvi/ps/pdf)
alias texC='latexmk -C -norc' # leave only *.tex files
# }}}
# {{{ _gnu_generic (i.e.: provides --help)
compdef _gnu_generic zathura
compdef _gnu_generic steamleaderboard
# }}}
# {{{Vim muscle memory
alias :q='exit'
alias :wq='exit'
alias :x='exit'
alias :e='$visual'
alias :sp='$visual -o'
alias :vsp='$visual -O'
# }}}
loopback() { # {{{
	pactl load-module module-loopback "source=$(
		pactl list sources | awk '/Name:/{print $2}' | fzf --prompt='Source: '
	)" "sink=$(
		pactl list sinks | awk '/Name:/{print $2}' | fzf --prompt='Sink: '
	)"
}
# }}}
# {{{ TODO: move to misc-scripts/make config file
alias SCKB='cp -v ~/.backup/Steam\ keyboard/layout_english_dualtouch.txt ~/.steam/tenfoot/resource/keyboards'
alias qr='qrencode -t UTF8'
fillscreen(){
	set -- "$*"
	print -rn ${(pl[COLUMNS*LINES][$1])}
	read -k1 _
}

# }}}

HISTORY_IGNORE="(#i)(cd|cd ..|clear|g ca#m *|(${(j:|:k)aliases[(R)ls*]})(| *)|exit)"
# vim: set foldmethod=marker:
