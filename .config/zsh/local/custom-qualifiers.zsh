zmodload zsh/attr
zmodload -F zsh/stat b:zstat

is{b,c,d,f,g,h,k,p,{r..x},L,O,G,S,N}(){
	case $zsh_eval_context[-2] in
		globqual) [ -${0:2} $REPLY ];;
		*) [ -${0:2} $1 ]
	esac
}

isa(){
	local -a a
	case $zsh_eval_context[-2] in
		globqual) ;;
		*) local REPLY=$1; shift ;;
	esac
	case $# in
	0)  # isa: Does the file have xattrs?
		zlistattr $REPLY a 2>/dev/null
		(($#a))
		;;
	1)  # isa $attr: Does the file have $attr?
		zgetattr $REPLY user.$1 a 2>/dev/null
		;;
	2)  # isa $attr $val: Does $attr match $val?
		local m=$2
		zgetattr $REPLY user.$1 a 2>/dev/null && [[ $a = $~m ]]
		;;
	*) return 1
	esac
}

path(){
	case $zsh_eval_context[-2] in
		globqual) set -- ${REPLY:a} ;;
		*)	set -- ${${1:-$PWD}:a}
	esac
	typeset -ga reply=($1)
	while [[ ${reply[1]} != / ]]; do
		reply=(${reply[1]:h} $reply)
	done
	: # return truthy
}
