KEYTIMEOUT=20 # 200 ms
(($+commands[less])) && READNULLCMD=less
SPROMPT='Correct %B%F{red}%U%R%b%f%u to %F{green}%r%f? [%By%bes|%BN%bo|%Be%bdit|%Ba%bbort] '
PS4='%F{black}: %F{magenta}%1N%f:%F{blue}%i%1(_.:%_.)%f; '
export BROWSER=xdg-open
export FZF_DEFAULT_OPTS="--bind=\"ctrl-j:preview-down,ctrl-k:preview-up,ctrl-alt-j:preview-page-down,ctrl-alt-k:preview-page-up,ctrl-a:select-all,ctrl-alt-a:deselect-all,alt-a:toggle-all,alt-e:execute($EDITOR {+})\""
export RIPGREP_CONFIG_PATH="$HOME/.config/rg.conf"
ZSH_AUTOSUGGEST_USE_ASYNC="true"
ZSH_EVIL_SYNC_EDITOR="$EDITOR"

# Bold files with multiple hardlinks
eval ${"$(TERM=xterm dircolors -b)"/mh=<0-0>:/mh=01:}
# vim: set foldmethod=marker:
