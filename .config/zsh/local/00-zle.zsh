# If we used zkbd to create a $key hash, load that first.
local zkbd=(${ZDOTDIR:-$HOME}/.zkbd/$TERM*(NY1))
if [[ -z $zkbd ]] || ! source $zkbd; then
	typeset -gA key
fi

zstyle :zle:evil-registers:'[A-Za-z%#=-]' editor $EDITOR

# Check if the terminal supports setting application mode.
zmodload zsh/terminfo
zmodload zsh/zutil
if [[ -n "$terminfo[smkx]" ]]; then
	# Enable application mode, so we can use terminfo codes.
	add-zle-hook-widget zle-line-init .zshrc.app-mode; .zshrc.app-mode() { echoti smkx }
	if [[ -n "$terminfo[rmkx]" ]]; then
		add-zle-hook-widget zle-line-finish .zshrc.raw-mode; .zshrc.raw-mode() { echoti rmkx }
	fi

	# Assign each value only if it has not been defined earlier.
	: ${key[Up]:=$terminfo[kcuu1]}    ${key[Down]:=$terminfo[kcud1]}
	: ${key[Right]:=$terminfo[kcuf1]} ${key[Left]:=$terminfo[kcub1]}
	: ${key[End]:=$terminfo[kend]}    ${key[Home]:=$terminfo[khome]}
	: ${key[PageUp]:=$terminfo[kpp]}  ${key[PageDown]:=$terminfo[knp]}
	: ${key[Return]:=$terminfo[cr]}   ${key[Delete]:=$terminfo[kdch1]}
	: ${key[Tab]:=$terminfo[ht]}      ${key[Backtab]:=${terminfo[kcbt]:-$terminfo[cbt]}}

	# F<1-> keycodes are in $terminfo[kf*]
	local k v
	for k v in ${(kv)terminfo[(I)kf*]}; do
		: ${key[F${k:2}]:=$v}
	done

	# Cursor keys
	local ti_k k_k fstr opt
	for ti_k k_k (cuu Up cud Down cuf Right cub Left); do
		# $terminfo[cuu] and similar should have a %d bitmask for Shift/Alt/Control.
		# Check if that is really the case:
		[[ $terminfo[$ti_k] = *'%d'* ]] || continue
		# SAC mask
		local -i i=1
		for opt (S A SA C CS CA CAS); do
			[[ -n $key[$opt$k_k] ]] && continue
			zformat -f "key[$opt$k_k]" "$terminfo[$ti_k]" p:'' d:';'$((++i))
		done
	done
fi

# Fill in any remaining blank values with common defaults.
: ${key[Up]:='^[[A'}   ${key[Down]:='^[[B'}    ${key[Right]:='^[[C'}   ${key[Left]:='^[[D'}
: ${key[End]:='^[[F'}  ${key[Home]:='^[[H'}    ${key[PageUp]:='^[[5~'} ${key[PageDown]:='^[[6~'}
: ${key[Return]:='\r'} ${key[Delete]:='^[[3~'} ${key[Tab]:='^I'}       ${key[Backtab]:='^[[Z'}

# Vi mode
bindkey -v

# Updates editor information when the keymap changes.
function .zshrc.cursor() {
	case $KEYMAP$ZLE_STATE in
		# block
		vicmd*) print -n "\e[2 q" ;;
		# bar
		*insert*) print -n "\e[6 q" ;;
		# underbar
		*) print -n "\e[4 q" ;;
	esac
}
.zshrc.set-keymap() {
	zle -K viins
}
autoload -Uz add-zle-hook-widget
add-zle-hook-widget zle-keymap-select .zshrc.cursor
add-zle-hook-widget zle-line-init .zshrc.set-keymap
sched +0 ZLE_STATE=insert .zshrc.cursor

# allow :e to edit the command line
autoload -Uz edit-command-line
if [[ $VISUAL = nvr* ]]; then
	edit-command-line-nvr(){ VISUAL="$VISUAL --remote-wait" edit-command-line }
	zle -N edit-command-line{,-nvr}
else
	zle -N edit-command-line
fi
local a k v m
for a in e ed edi edit; do
	zle -A edit-command-line $a
done

# simple fzf history search
if (($+commands[fzf])); then
	my-fzf-history-search(){
		local query=$(fc -rl 1 | fzf +m -n2..,.. --tiebreak=index --query=$BUFFER)
		local -i idx=${(M)query##( #[[:digit:]]##)}
		((idx)) && zle vi-fetch-history -n $idx
	}
	zle -N my-fzf-history-search
fi

# simple help page widget
-zle-man(){
	man ${=WIDGET#*man-}
	zle redisplay
}
zle -N man-zshall -zle-man

# {{{ Bind in all keymaps
for m (vicmd viins visual) for k v (

	# Ctrl+Space: Either push this line onto the stack, or move secondary text into the main buffer
	'^ ' push-line-or-edit

	# allow ctrl-p, ctrl-n for navigate history (standard behaviour)
	'^P' history-incremental-pattern-search-backward
	'^N' history-incremental-pattern-search-forward

	# Ctrl/Shift+Left/Right: word movement
	"$key[CRight]" vi-forward-shell-word
	"$key[SRight]" vi-forward-word
	"$key[CLeft]"  vi-backward-shell-word
	"$key[SLeft]"  vi-backward-word

	# Control/Shift+Up/Down: history search
	"$key[CUp]" my-fzf-history-search
	"$key[SUp]" history-beginning-search-backward
	"$key[SDown]" history-beginning-search-forward

	# F1: zsh help
	"$key[F1]" man-zshall
) {
	bindkey -M "$m" $k $v
}
# }}}
# {{{ viins only

# ctrl-r paste
bindkey '^R' →evil-registers::ctrl-r

# allow ctrl-h, ctrl-w, ctrl-? for char and word deletion (standard behaviour)
bindkey '^?'    backward-delete-char
bindkey '^h'    backward-delete-char
bindkey '^[[3~' delete-char
bindkey '^w'    backward-kill-word

# allow ctrl-a and ctrl-e to move to beginning/end of line
bindkey '^a' beginning-of-line
bindkey '^e' end-of-line

# }}}
# {{{ vicmd
# pipe a text selected by a motion through another command
autoload -Uz vi-pipe
zle -N vi-pipe
bindkey -M vicmd '!' vi-pipe

# Sensible
bindkey -M vicmd 'Y' vi-yank-eol

# Vim tags run-help
bindkey -M vicmd '\C-]' run-help

for m in vicmd visual; do
	bindkey -M "$m" 'W'  vi-forward-shell-word
	bindkey -M "$m" 'B'  vi-backward-shell-word
	bindkey -M "$m" 'E'  vi-forward-shell-word-end
	bindkey -M "$m" 'gE' vi-backward-shell-word-end
done
# }}}
# {{{ Completion
# These are tried in order until one of them succeeds:
# _oldlist: if we already have completions, try to reuse those
# _expand: expansion substitutions (See http://zsh.sourceforge.net/Doc/Release/Expansion.html)
# _complete: regular completions
# _history: words from history
# _correct: spelling corrections
# _ignored: any completions that have been ignored so far
zstyle ':completion:*' completer _oldlist _expand _complete _correct _ignored #_history

# Don't generate new completions if the previous list is still showing.
zstyle ':completion:*' old-list shown

zstyle ':completion:*' range 1000:100 # Try 100 history words at a time; max 1000 words.
zstyle ':completion:*' remove-all-dups yes  # Don't suggest duplicate history words.

# Each 'string in quotes' is tried in order until one them succeeds.
# r:|[.]=** does .bar -> foo.bar
# r:?|[-_]=** does f-b -> foo-bar and F_B -> FOO_BAR
# l:?|=[-_] does foobar -> foo-bar and FOOBAR -> FOO_BAR
# m:{[:lower:]-}={[:upper:]_} does foo-bar -> FOO_BAR
# r:|?=** does fbr -> foo-bar and bar -> foo-bar
zstyle ':completion:*:complete:*' matcher-list \
  'r:|[.]=** r:?|[-_]=** l:?|=[-_] m:{[:lower:]-}={[:upper:]_}' \
  'r:|?=** m:{[:lower:]}={[:upper:]}'

# For options only, add - -> +
zstyle ':completion:*:options' matcher 'b:-=+'

# Ignore functions, parameters and users starting with punctuation.
#zstyle ':completion:*:(functions|parameters|users)' ignored-patterns '[[:punct:]]*[[:alnum:]]*'

# Enable grouping and show group titles.
zstyle ':completion:*' group-name ''
zstyle ':completion:*:*:-command-:*' group-order aliases functions builtins reserved-words commands
zstyle ':completion:*' group-order corrections original expansions all-expansions
zstyle ':completion:*:descriptions' format $'➤ %B%d%b (%B%F{cyan}%n%f%b)'  # newlines cause segfault

# Make messages and warnings a bit nicer.
zstyle ':completion:*:messages' format "%F{r}%d%f"
zstyle ':completion:*:warnings' format '%F{r}No %d completions found.%f'

# Show and insert `man` sections.
zstyle ':completion:*' insert-sections yes
zstyle ':completion:*' separate-sections yes

# Load support for colored completions, list scrolling and menu selection.
# See http://zsh.sourceforge.net/Doc/Release/Zsh-Modules.html#The-zsh_002fcomplist-Module
zmodload zsh/complist
autoload -Uz colors; colors   # Initialize associative array $color

zstyle ':completion:*:default' list-prompt '' # Enable completion list scrolling.

# Press Tab to starts type-ahead completion search.
# In the search:
#   * Press Tab to insert the value shown by `interactive: <prefix>[]<suffix>`.
#   * Press Enter to insert the value that's currently selected.
#   * Use the arrows keys to change the selection.
setopt MENU_COMPLETE
zstyle ':completion:*:default' menu yes select

# Highlight next differentiating character (breaks LS_COLORS)
# zstyle ':completion:*:default' show-ambiguity "$color[white]"
zstyle ':completion:*' list-separator '-'  # Eliminates some false positives.
zstyle ':completion:*' list-dirs-first true

# Reuse $LS_COLORS for completion highlighting and add additional colors.
# zstyle -e: Evaluate the given code each time this style gets checked.
# See http://zsh.sourceforge.net/Doc/Release/Zsh-Modules.html#Colored-completion-listings
zstyle -e ':completion:*' list-colors 'reply=(
		"${(s<:>)LS_COLORS}"
		"sa=$color[green];$color[underline]"
		"(global-aliases|parameters)=*=$color[cyan]"
		"(aliases|executables|functions|commands|builtins|jobs)=*=$color[green]"
		"(reserved-words)=*=$color[yellow];$color[bg-black]"
		"(glob(flags|quals)|modifiers)=*=$color[blue]"
	)'
#zstyle ':completion:*:options' list-colors '=(#b)(*)-- (*)=1=34'
zstyle ':completion:*:options' list-colors '=(#b)(-[^ -]#)#( [^-]*)=0=0=33' #'=*=31'

# Enable completion caching & store cache files in a sensible place.
zstyle ':completion:*' use-cache yes
zstyle ':completion:*' cache-path ${ZDOTDIR:-$HOME}/.zcompcache

# Finally, initialize the completion system.
# See http://zsh.sourceforge.net/Doc/Release/Completion-System.html#Use-of-compinit
autoload -Uz compinit; compinit -d ${ZDOTDIR:-$HOME}/.zcompdump

# Better control in menu
bindkey -M menuselect '^d' forward-word
bindkey -M menuselect '^u' backward-word
bindkey -M menuselect '^I' forward-char
bindkey -M menuselect '^[[Z' backward-char
# Navigate sections
bindkey -M menuselect '^N' vi-forward-blank-word
bindkey -M menuselect '^P' vi-backward-blank-word
# Leave
bindkey -M menuselect '^[' send-break
zstyle -e ':completion:*:(mosh|ssh|scp|sftp|rsh|rsync):*:hosts' hosts .zsh.hosts
.zsh.hosts(){
	typeset -gaU reply=(
		${${(M)${(f)"$(cat ~/.ssh/config)"}:#Host *}#Host }
		${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|<0->)(N) /dev/null)"}%%[# ]*}//,/ }
	)
}

# Autocomplete on hidden(dot) files
_comp_options+=(globdots)

# }}}
# vim: set foldmethod=marker:
