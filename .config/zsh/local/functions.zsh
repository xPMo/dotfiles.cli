#!/usr/bin/env zsh
fpath+=($ZDOTDIR/functions)
disable -a run-help which-command
setopt extendedglob # Why is this necessary?
autoload -Uz - $ZDOTDIR/functions/^*.(md|zwc)(D:t) \
	zcalc zmv zargs run-help which-command add-zsh-hook zed

# Dynamic directory name, uses functions/zdn_*
zsh_directory_name_functions+=(zdn_{vcs,mount} zdn_generic_alternate{,_complete})
zstyle -e :zdna:m dirs 'reply=(${(f)"$(findmnt -lno target)"})'
zstyle    :zdna:m match-by nofix prefix infix
zstyle -e :zdna:g dirs 'reply=($gitrepopath)'
zstyle    :zdna:g match-by sub nofix prefix infix

# efn supports loading a function into the buffer when called as a widget
zle -N efn

# Steal completions for custom scripts/functions
sched +0 compdef __tmux-sessions tmax
sched +0 compdef _functions efn hifn
sched +0 compdef _aliases eal
sched +0 compdef _grc grc
sched +0 compdef _time_zone tz
sched +0 compdef _files d D
sched +0 compdef _mpv listento='mpv --shuffle --no-video'
sched +0 compdef _parameters printkv

(){
	# Lazy copy autoloaded functions when needed
	# There are some functions (both mine and zshcontrib) which use the same function body,
	# but their behavior depends on $0 (their name). Normally we couldn't set these to autoload,
	# but if we define the function as:
	# newname(){
	#	functions -c oldname newname
	#	newname "$@"
	# }
	# We force oldname to be loaded and override newname in the same step.
	local a b
	for a b (
		sdu du
		'zcp zln' zmv
		D d
		foldl-reply foldl
	);{
		b='functions -c '$b' $0;$0 "$@"'
		for a ($=a);
			functions[$a]=$b
	}
}

# title / tabs
.set-title(){
	setopt localoptions banghist
	local c
	(($+1)) && c=$1
	local d=("${(@s[/])${(%):-%~}}")
	if (($#d < 3)); then
		d=${(j:/:)d}
	else
		d=$d[1]/‥/$d[-1]
	fi
	if [[ -v TMUX ]]; then # just command, OR path
		#printf '\e]0;%s\a' "${${c:0:10}:-$d}"
		printf '\e]2;%s\e\\' "${${c:0:10}:-$d}"
		printf '\ek%s\e\\'   "${${c:0:10}:-$d}"
	else # both command AND path
		printf '\e]0;%s\a' "[${SSH_CONNECTION+${(%)%n@%M}:}$d] ${${c:0:10}:-zsh}${${c:10}:+‥} — ${(C)${TERM_PROGRAM:-$TERM}}"
	fi
}
add-zsh-hook preexec .set-title
add-zsh-hook precmd  .set-title

# Set tabstops using raw $terminfo/$termcap codes
if (($+terminfo)); then
	.on_winch(){
		# tabs
		local -a s=(${${terminfo[cuf]//(\%i|\%p1)}/\%d/$1}$terminfo[hts])
		repeat COLUMNS/$1 s+=($s[1])

		# final
		print -rn $terminfo[sc]${terminfo[hpa]//(\%i|\%p1|\%d)}$terminfo[tbc]${(j::)s}$terminfo[rc]
	}
	.on_winch 4
	trap '.on_winch 4' WINCH

elif (($+termcap)); then
	.on_winch(){
		# tabs
		local -a s
		repeat COLUMNS/$1 s+=(${termcap[RI]/\%p1\%d/$1}$termcap[st])

		# final
		print -rn $termcap[sc]${termcap[ch]//(\%i|\%p1|\%d)}$termcap[ct]${(j::)s}$termcap[rc]
	}
	.on_winch 4
	trap '.on_winch 4' WINCH
fi

# Anonymous functions should be used over this,
# but they don't work with a higher order function like zargs or foldl.
#
#   lambda 'echo $#; print "${(j<, >)@}"' *
#
λ lambda snippet(){
	eval "shift; $1"
}

#   bin2ascii 01110100 01101000 01100101 00100000 01100111 01100001 01101101 01100101 00001010
bin2ascii(){
	printf %s ${(#)@/#/2#}
}

# This formats hyperlinks in $reply. Usage:
#
#   hyperlink 'https://github.com/xPMo' Github 'https://gitlab.com/xPMo' Gitlab
#   print -rl 'Find me at:' $reply
#
hyperlink(){
	if ((! $# || $# % 2)); then
		print -u2 "Usage: $0 [URL] [TEXT] [ [URL] [TEXT] ... ]"
		return 1
	fi
	typeset -ga reply=()
	printf -v reply '\e]8;;%s\a%s\e]8;;\a%s' "$@"
}

# Quote and print all arguments.
showargs() {
	printf '%d arguments\n' $#
	print -r - "[ ${(j<, >)${(q+)@}} ]"
}

# Attempt to rerun a program from its PID by pulling as much state from /proc as possible.
rerun(){
	local pid=${1:?Missing PID}
	local -a reply env=(${(@ps[\0])"$(</proc/$pid/environ)"})
	zstat -A reply -L /proc/$pid/cwd
	pushd $reply[14]
	set -- "${(@ps[\0])"$(</proc/$pid/cmdline)"}"
	kill -- $pid
	command env -i - "${(@)env}" "$@"
	popd
}
compdef _kill rerun

# One character aliases/functions are nice, show what I've defined and what other names I have available.
1 onechar(){
	local free=({a..z} {A..Z})
	local i
	for i (aliases functions builtins commands){
		local l=(${(k)${(P)i}[(I)?]})
		free=(${free:|l})
		print "${(r:11:)i} ${(l:2:)#l}  ${(j< >)l}"
	}
	print "remaining   ${(l:2:)#free}  ${(j< >)free}"
}

with with0 wit0(){
	# Compare:
	# - with locate '*.ogg' do mpv
	# - mpv ${(f)"$(locate '*.ogg')"}

	# - with0 locate -0 '*.ogg' do mpv
	# - mpv ${(0)"$(locate -0 '*.ogg')"}

	# - with some-command
	# - reply=(${(f)"$(some-command)"})

	# Can't accomodate $( .. | .. ), but I guess
	# it can be composed with 'snippet' or 'eval'
	local -i do=$@[(i)do]
	local -a src=("${@[1,do - 1]:/-do/do}")
	if ((do < $#)); then
		shift do
		local -a reply=()
	else
		set --
		typeset -ga reply=()
	fi
	case $0 in
		*0) reply=(${(0)"$("${(@)src}")"}) ;;
		*) reply=(${(f)"$("${(@)src}")"})
	esac
	(($# && do)) && "$@" "${(@)reply}"
	return 0
}


# math functions
autoload zmathfunc
zmathfunc  # loads min, max, sum
functions -M fact 1 1 zsh_math_func_fact
functions -M combi 2 2 zsh_math_func_combi
functions -M prod 0 -1 zsh_math_func_prod
functions -M avg 1 -1 zsh_math_func_avg
functions -M srandom 0 0 srandom
alias -g '$srandom=$((srandom()))'  # hack to approximate Bash's special $SRANDOM variable
functions -Ms numfmt 1 1 zsh_math_func_numfmt
functions -Ms print 1 1 zsh_math_func_print
functions -Ms roll 1 1 roll
