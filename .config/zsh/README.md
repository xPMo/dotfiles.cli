# Zsh Config

## `.zshenv`

This should be symlinked into `~`. Through some `${(%)...}` magic, it sets this directory as `$ZDOTDIR` as well as a few global environment variables.

## `.zshrc`

- Catches ssh connections and connects them to `tmux` or `dtach`
- Loads Powerlevel10k instant prompt
- Sets my preferred options
- Loads zsh-defer
- Loads everything in `plugins/` and `local/` with zsh-defer if supported.

## `local/`

The rest of my zsh config is split into files in this directory, including my powerlevel10k config.

## `functions/`

Bigger functions I've written live here, they get `autoload`ed by `local/functions.zsh`.
