setopt warncreateglobal
# {{{ SSH multiplexing
case ${SSH_CONNECTION++}${DTACH+:}$TERM in
	+:*|+screen*|+tmux*) :;; # don't nest
	+*)
		if (( $+commands[tmux] )); then (){
			local target
			target="$(tmux list-sessions -F "#S" 2>/dev/null)"
			if ! (( $? )) ; then
				target="$(tmux new-session -t "${target%%$'\n'*}")"
				[[ "$target" = \[detached* ]] && tmux kill-session -t "${target:24: -2}"
			else tmux new-session; fi
		}
		elif (( $+commands[dtach] )); then
			export DTACH="${XDG_RUNTIME_DIR:-$PREFIX/tmp}/$TTY-dtach"
			dtach -A "$DTACH" zsh
			unset DTACH
		fi
esac
# }}}
# {{{ Termux-specific
if (( ${+ANDROID_DATA} )); then
	:
else
	:
fi
# }}}
# {{{ P10k instant prompt
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# }}}
. $ZDOTDIR/.zprofile

# functions needed for setup
autoload -Uz add-zsh-hook add-zle-hook-widget is-at-least
# {{{ Interactive vars/options
SAVEHIST=200000
HISTSIZE=200000
: ${HISTFILE="$ZDOTDIR/zsh_history"}

# cd/pushd
setopt autocd autopushd
# zle parsing and globbing
setopt promptsubst interactivecomments rcquotes rcexpandparam extendedglob globstarshort cbases octalzeroes
# history
setopt extended_history hist_expire_dups_first hist_ignore_dups hist_ignore_space hist_verify share_history inc_append_history
# others
setopt noclobber correct
#PROMPT_EOL_MARK='%F{red}🅽%f'
PROMPT_EOL_MARK='%U%B%F{red}⮒%f%b%u'

# }}}
# {{{ Plugins

# p10k can't be deferred
defer_blacklist=(
	me--dir-glob
	me--dir-perms
	p10k
	romkatv--zsh-defer
	romkatv--powerlevel10k
)

# First round
for file (
	$ZDOTDIR/plugins/(${(~j:|:)defer_blacklist})/*.(plugin.zsh|zsh-theme)
	$ZDOTDIR/local/(${(~j:|:)defer_blacklist})(.zsh|)
); do
	source $file
done

# for better widget debugging:
#defer_blacklist+=(zdharma--fast-syntax-highlighting)
defer_blacklist+=(zsh-users--zsh-autosuggestions)
#defer_blacklist+=(zvm--reg)
#defer_blacklist+=(zvm--motions)

for file (
	$ZDOTDIR/local/^(${(~j:|:)defer_blacklist})(.zsh|)(#qN)
	$ZDOTDIR/plugins/^(${(~j:|:)defer_blacklist})/*.(plugin.zsh|zsh-theme)(#qN)
	/usr/share/fzf/completion.zsh(#qN)
); do
	zsh-defer source $file
done
unset defer_blacklist file

# }}}

eval $startup
eval $STARTUP
# vim: set foldmethod=marker:
