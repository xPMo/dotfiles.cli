syntax match pythNumber /\v\d+/
syntax region pythString start=/"/ skip=/\\"/ end=/\v"|$/
syntax match pythCharacter /\v\\./
syntax match pythControl /[#);BFIVW&|?]/
syntax match pythControl "\.?"
syntax match pythControl "\.W"
syntax match pythControl "\.x"
syntax match pythStructure /[[({,\]]/
syntax match pythStructure ".{"
syntax match pythAssignment /[~=ADLMX]/
" match first instance of J and K
syntax match pythAssignment /\v(^[^J]*)@<=J/
syntax match pythAssignment /\v(^[^K]*)@<=K/
syntax match pythArithmetic /[+*-/%\^_P]/

highlight default link pythNumber Number
highlight default link pythString String
highlight default link pythControl Keyword
highlight default link pythStructure Structure
highlight default link pythCharacter Character
highlight default link pythAssignment PreProc
highlight default link pythArithmetic Function
