#!/usr/bin/env bash
case ${SSH_CONNECTION++}${DTACH+:}$TERM in

+:*|+tmux*|+screen*) ;;
+*)
	if command -v tmux >/dev/null; then
		if tmux has-session 2>/dev/null; then
			ss="$(tmux list-sessions -F "#S")"
			ret="$(tmux new-session -t "${ss%%$'\n'*}")"
			[[ "$ret" = '[detached'* ]] && tmux kill-session -t "${ret:24: -2}"
			unset ss ret
		else
			tmux new-session
		fi
	elif command -v dtach >/dev/null; then
		export DTACH=${XDG_RUNTIME_DIR:-$PREFIX/tmp/dtach-$USER}/dtach.sock
		dtach -A "$DTACH" "$SHELL"
		unset DTACH
	fi

esac
