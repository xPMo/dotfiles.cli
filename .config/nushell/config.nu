# config.nu
#
# Installed by:
# version = "0.101.0"
#
# This file is used to override default Nushell settings, define
# (or import) custom commands, or run any other startup tasks.
# See https://www.nushell.sh/book/configuration.html
#
# This file is loaded after env.nu and before login.nu
#
# You can open this file in your default editor using:
# config nu
#
# See `help config nu` for more options
#
# You can remove these comments if you want or leave
# them for future reference.

$env.config = ($env.config? | default {} | merge {
	show_banner: false,
	edit_mode: 'vi',
	cursor_shape: {
		vi_insert: line
		vi_normal: block
		emacs: block
	}
})

use std/math
use std/log
use std/dirs
use std/iter


alias _ = sudo
# cat
alias c = open
alias o = open
alias h = history
alias g = ^git
alias m = ^man
alias N = exec nu
alias p = ^wl-paste
alias P = ^wl-paste -p
alias y = ^wl-copy
alias Y = ^wl-copy -p

alias l  = ls -l
alias la = ls -a

def --wrapped mounts [...rest] {
	^findmnt --output-all --json | (from json).filesystems.0
}
def --wrapped ip [...rest] {
	^ip -j ...($rest) | from json
}

def --wrapped swaymsg [...rest] {
	^swaymsg -r ...($rest) | from json
}

def --wrapped lsblk [...rest] {
	^lsblk -J ...($rest) | from json
}

# TODO: Parse Optional Deps correctly
# - can't use 'lines | reduce ...', have to be more custom than that
def listpkgs [...packages] {
	^pacman -Si ...($packages) | str trim | split row "\n\n" | each {
		lines | reduce --fold {} {|row, rec|
			let row = ($row | split row -r ' : ' | str trim)
			if ($row | length) < 2 or $row.1 == 'None' {
				$rec
			} else if $row.0 in [ 'Groups', 'Provides', 'Depends On', 'Optional Deps', 'Conflicts With', 'Replaces', 'Licenses'] {
				$rec | insert $row.0 ($row.1 | split row -r '\s+')
			} else {
				$rec | insert $row.0 $row.1
			}
		}
		}
}
