
# ==========================================================
# Various system control settings
# ==========================================================

# Toggle i3bar
bindsym $mod+Menu bar mode toggle bar1

# Toggle touchpad
bindsym XF86TouchpadToggle input type:touchpad events toggle enabled disabled, \
	exec notify-send Touchpad "$(swaymsg -t get_inputs | jq -r '.[]|select(.type == \"touchpad\").libinput.send_events | (.[:1] | ascii_upcase) + .[1:]')"
bindsym $mod+Shift+Menu input type:touchpad events toggle enabled disabled, \
	exec notify-send Touchpad "$(swaymsg -t get_inputs | jq -r '.[]|select(.type == \"touchpad\").libinput.send_events | (.[:1] | ascii_upcase) + .[1:]')"

# {{{ Notifications: Mako
bindsym $mod+Delete exec makoctl dismiss
bindsym $mod+Shift+Delete exec makoctl dismiss --all
bindsym $mod+Control+Delete exec makoctl reload
bindsym $mod+Home exec makoctl restore
bindsym $mod+Shift+Home exec makoctl menu wofi -dp 'Select action'
# }}}
# {{{ Backlights
bindsym XF86MonBrightnessDown exec --no-startup-id \
	brightnessctl -c backlight set 2%-
bindsym XF86MonBrightnessUp exec --no-startup-id \
	brightnessctl -c backlight set +2%
bindsym Shift+XF86MonBrightnessDown exec --no-startup-id \
	brightnessctl -c backlight set 4%
bindsym Shift+XF86MonBrightnessUp exec --no-startup-id \
	brightnessctl -c backlight set 100%
# }}}
# F1: Show i3 information
bindsym $mod+F1 exec --no-startup-id i3hint version workspace layout
bindsym $mod+Shift+F1 exec --no-startup-id i3hint help

# {{{ LOGIN AND POWER:

# F2: Reload or restart inplace
bindsym $mod+F2 reload
bindsym $mod+Shift+F2 restart

# F3: lock/leave session
bindsym $mod+F3 exec loginctl lock-session $XDG_SESSION_ID
#bindsym $mod+Shift+F3 exec --no-startup-id "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

bindsym XF86Sleep exec systemctl suspend
bindsym Shift+XF86Sleep exec systemctl hybrid-sleep

# F3: 
# $mod+F4: system
set $mode_system <small><b>L</b>ock, <b>E</b>xit, switch_<b>U</b>ser, <b>S</b>uspend, <b>H</b>ibernate, <b>R</b>eboot, <b>⇧+S</b>hutdown</small>
bindsym XF86PowerOff mode "$mode_system"
bindsym $mod+F4 mode "$mode_system"
mode --pango_markup "$mode_system" {
    bindsym l exec loginctl lock-session $XDG_SESSION_ID, mode "default"

    bindsym s exec systemctl suspend, mode "default"
    bindsym u exec dm-tool switch-to-greeter, mode "default"
    bindsym e exit
    bindsym h exec systemctl hibernate, mode "default"
    bindsym r exec systemctl reboot, mode "default"
    bindsym Shift+s exec systemctl poweroff, mode "default"

    # exit system mode: "Enter" or "Escape"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
# }}}
# vim: set foldmethod=marker:
