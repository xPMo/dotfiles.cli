! {{{ Define Solarized colors
#define S_base03        #002b36
#define S_base02        #073642
#define S_base01        #586e75
#define S_base00        #657b83
#define S_base0         #839496
#define S_base1         #93a1a1
#define S_base2         #eee8d5
#define S_base3         #fdf6e3

#define S_yellow        #b58900
#define S_orange        #cb4b16
#define S_red           #dc322f
#define S_magenta       #d33682
#define S_violet        #6c71c4
#define S_blue          #268bd2
#define S_cyan          #2aa198
#define S_green         #859900

#define S_lyellow       #cccc00
#define S_lblue         #2c98e4
#define S_lcyan         #30b8b0
#define S_lgreen        #85c000

#define S_base04        #001014
#define S_dred          #9c2220
#define black           #000000
! }}}
! {{{ xft
Xft*antialias:          true
Xft*hinting:            medium
! }}}
! {{{ Colors
*background:            S_base04
*foreground:            S_base3
*fadeColor:             [65]#000000
*cursorColor:           S_base1
*pointerColorBackground:S_base02
*pointerColorForeground:S_base2

!! black dark/light
*color0:                S_base03
*color8:                S_base01

!! red dark/light
*color1:                S_red
*color9:                S_orange

!! green dark/light
*color2:                S_green
*color10:               S_lgreen

!! yellow dark/light
*color3:                S_yellow
*color11:               S_lyellow

!! blue dark/light
*color4:                S_blue
*color12:               S_lblue

!! magenta dark/light
*color5:                S_magenta
*color13:               S_violet

!! cyan dark/light
*color6:                S_cyan
*color14:               S_lcyan

!! white dark/light
*color7:                S_base1
*color15:               S_base2
! }}}
! {{{ Urxvt
urxvt*termName:             rxvt-unicode-256color

urxvt*scrollBar:            false
urxvt*perl-lib:             /usr/lib/urxvt/perl/
urxvt*perl-ext-common:      default,url-select
urxvt*url-select.launcher:  /usr/bin/xdg-open
urxvt*url-select.underline: true
urxvt*fading:               30
URxvt.keysym.M-u:           perl:url-select:select_next
URxvt.keysym.C-S-c:         perl:clipboard:copy
URxvt.keysym.C-S-v:         perl:clipboard:paste

urxvt*depth:                32
urxvt*background:           [90]S_base04

!urxvt.transparent:      true
!urxvt.tintColor:        black
!urxvt.shading:          20

! Used with https://github.com/majutsushi/urxvt-font-size
urxvt.keysym.C-Up:      font-size:increase
urxvt.keysym.C-Down:    font-size:decrease
urxvt.keysym.C-S-Up:    font-size:incglobal
urxvt.keysym.C-S-Down:  font-size:decglobal
urxvt.keysym.C-equal:   font-size:reset
urxvt.keysym.C-slash:   font-size:show
urxvt.font-size.step:   0.5
! }}}
#include ".Xresources.local"
! vim: set foldmethod=marker:
