if [ -z "${_PROFILE_SOURCED+1}" ]; then

export XIVIEWER=imv
# shellcheck disable=1090
case "${XDG_SESSION_DESKTOP-}" in
	*i3*) # use i3lock on suspend
		export DMENU='rofi -i -dmenu'
		light-locker --no-lock-on-suspend & ;;
	*sway*)

		#: "${XDG_CURRENT_DESKTOP:=KDE}"
		#export WLR_NO_HARDWARE_CURSORS=0
		export CLUTTER_BACKEND=wayland
		export DMENU='wofi -i -d'
		export MOZ_ENABLE_WAYLAND=1
		export PASH_CLIP='wl-copy'
		#export QT_QPA_PLATFORM=wayland
		export QT_QPA_PLATFORMTHEME=qt6ct
		export SDL_VIDEODRIVER=wayland
		export WLR_NO_HARDWARE_CURSORS=1
		export WLR_RENDERER_ALLOW_SOFTWARE=1
		export XCURSOR_THEME='Bibata-Modern-Classic'
		export XDG_SESSION_TYPE=wayland
		export XDG_CURRENT_DESKTOP=sway
		;;
	?*)
		light-locker --lock-on-suspend & ;;
esac
if [ -n "${DESKTOP_SESSION+x}" ]; then
	export XIVIEWER=imv
	command -v kvantummanager >/dev/null && export QT_STYLE_OVERRIDE=kvantum
	command -v gnome-keyring-daemon >/dev/null && eval "$(gnome-keyring-daemon --start)"
	command -v light-locker >/dev/null && light-locker --lock-on-suspend
fi

[ -n "${ZSH_ARGZERO:-}" ] && setopt +o nomatch
umask 027

export GOPATH="$HOME/.local/share/go"

# prepend paths
# fallback: bin ⇒ guibin ⇒ clibin
for d in \
	"$HOME/.cargo/bin" "$HOME/bin" "$HOME/.local/clibin" "$HOME/.local/guibin" "$HOME/.local/bin"
do
	case $PATH in (*"$d"*)
		continue
	esac
	[ -d "$d" ] && PATH="${d}:$PATH"
done
export PATH

# prepend git repo paths
if [ -h "$HOME/.zshenv" ]; then
	zdotdir="$(realpath "$HOME/.zshenv")"
	zdotdir="${zdotdir%/*}"
fi
for d in \
	"${XDG_DATA_HOME:-"$HOME/.local/share"}/nvim/site/pack/git"*'/start' \
	${zdotdir+"$zdotdir/plugins"} "$HOME/Pictures/inkscape-repos" \
	"$HOME/Documents/current/repos"* "$HOME/Repos"*
do
	case ":$GITREPOPATH:" in
        *:"$d":*) ;;
        ::) [ -d "$d" ] && GITREPOPATH="${d}" ;;
        *) [ -d "$d" ] && GITREPOPATH="${d}:$GITREPOPATH" ;;
    esac
done
export GITREPOPATH

esc=$(printf '\033')
if command -v less >/dev/null 2>&1; then
	export LESS_TERMCAP_mb="${esc}[1;31m" LESS_TERMCAP_md="${esc}[1;31m" LESS_TERMCAP_me="${esc}[0m" LESS_TERMCAP_se="${esc}[0m" LESS_TERMCAP_ue="${esc}[0m" LESS_TERMCAP_us="${esc}[1;32m"
	export PAGER=less
	export LESS='--tabs=4 --no-init'
	export _NROFF_U=1
fi

export PASSWORD_STORE_ENABLE_EXTENSIONS=true
export PASH_KEYID='Gamma <GammaFunction@vivaldi.net>'
export _PROFILE_SOURCED=''

unset d zdotdir esc
fi
# /etc/profile
