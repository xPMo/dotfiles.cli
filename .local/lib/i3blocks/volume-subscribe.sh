#!/usr/bin/env bash

[[ -n "$1" ]] && set -x

pactl subscribe | {
	pactl list sinks
	while read -r line; do
		[[ $line = *sink* ]] || continue
		pactl list sinks
		printf 'P\n'
	done
} | awk '
/^Sink/{
	sink=$2
}
/\s*Mute:/{
	mute[sink]=$2
}
/^\s*Volume:/{
	volume[sink]=$5
}
# print
/^P/{
	for (i in mute){
		if (sink){
			sink=""
		} else {
			printf("<span size=\"small\"> </span>")
		}
		if (mute[i] == "yes") {
			printf("<span foreground=\"#b58900\">%s</span>",volume[i])
		} else {
			printf("%s", volume[i])
		}
		delete mute[i]
		delete volume[i]
	}
	print ""
}
' &
pid=$!

# requires i3blocks@6e8b51d or later
while read -r button; do
	# shellcheck disable=1091
	case "$button" in
		1) pactl set-sink-mute @DEFAULT_SINK@ toggle ;;
		3) . sys-notif media ;;
		4) pactl set-sink-volume @DEFAULT_SINK@ +1024;;
		5) pactl set-sink-volume @DEFAULT_SINK@ -1024;;
	esac
done

kill $pid
