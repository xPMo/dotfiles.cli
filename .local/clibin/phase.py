#!/usr/bin/env python3

import math
import os
import time
import signal
import sys

cols = 80
def winsize(signum, frame):
    global cols
    try:
        cols = os.get_terminal_size().columns
    except:
        pass
winsize(None, None)

signal.signal(signal.SIGWINCH, winsize)

count = 9
PI = math.pi
dx = 0.04
dy = 1.2
x  = 0

while True:
    # Exact values
    ys = [dy / 2 + (cols - dy) * (1 + math.sin(x - 2 * PI * phase / count)) / 2 for phase in range(count)]

    # Use dy for close matches
    print(''.join(["*" if any(abs(y + 0.5 - y1) < dy for y1 in ys) else " " for y in range(cols)]))
    time.sleep(0.1)
    x += dx
